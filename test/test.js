
const Worf = require('../lib/Worf');
const worf = new Worf();
const SECRET = 'dfsfsdfsdf';
const SECRET_OW = 'htrhgff';

var users = [
    {
        token : null,
        data : { id : 1, role : 'standard' },
        decoded : null,
    },
    {
        token : null,
        data : { id : 2, role : 'moderator' },
        decoded : null,
    },
    {
        token : null,
        data : { id : 3, role : 'admin' },
        decoded : null,
    }
];


worf.config({
    secret : SECRET,
    lists : {
        role : ['standard', 'moderator', 'admin', 'super']
    }
})

function req(userToken, options) {
    options = options || {};
    var req = {};
    req.url = "http://www.url.mock";

    if(options.cookie !== false) req.cookies = { token : userToken };
    if(options.header !== false) req.headers = { authorization : userToken };
    if(options.query !== false) req.url += '?token='+userToken;
    return req;
}

function res(){
    return {
        statusCode : 200,
        end : () => { }
    }
}

function auth(userToken, options, reqOptions, r = res()){
    return worf.authenticate(options)(req(userToken, reqOptions), r, ()=>{});
}

describe('JWT', function() {

    it('should create JWT for all users', function(done) {
        for(var i=0; i<users.length; i++) {
            users[i].token = worf.encode(users[i].data);
            if(!users[i].token) return done(new Error('At least one JWT was not created'));
        }

        done()
    })

    it('should decode all JWTs and add the token to the returned object', function(done) {
        for(var i=0; i<users.length; i++) {
            users[i].decoded = worf.decode(users[i].token);
            if(!users[i].decoded) return done(new Error('At least one JWT was not decoded'))

            for(var x in users[i].data) {
                if(!users[i].decoded[x]) return done(new Error('Field ' + x + ' is not in decoded object'));
            }

            if(!users[i].decoded.token) return done(new Error('Token was not added to decoded object'));
        }
        done()
    })

})


describe('getUser method', function() {

    it('should get user from request via cookie', function(done) {
        var request = req(users[0].token, { cookie : true, header : false, query : false});
        var user = worf.getUser(request);
        if(!user) return done(new Error('User not parsed'))
        if(user.id != users[0].data.id) return done(new Error('Ids do not match'));
        if(user.role != users[0].data.role) return done(new Error('Roles do not match'));
        done();
    })

    it('should get user from request via header', function(done) {
        var request = req(users[0].token, { cookie : false, header : true, query : false});
        var user = worf.getUser(request);
        if(!user) return done(new Error('User not parsed'))
        if(user.id != users[0].data.id) return done(new Error('Ids do not match'));
        if(user.role != users[0].data.role) return done(new Error('Roles do not match'));
        done();
    })

    it('should get user from request via query', function(done) {
        var request = req(users[0].token, { cookie : false, header : false, query : true});
        var user = worf.getUser(request);
        if(!user) return done(new Error('User not parsed'))
        if(user.id != users[0].data.id) return done(new Error('Ids do not match'));
        if(user.role != users[0].data.role) return done(new Error('Roles do not match'));
        done();
    })

    it('should not get user from request without token', function(done) {
        var request = req(users[0].token, { cookie : false, header : false, query : false});
        var user = worf.getUser(request);
        if(user) return done(new Error('User was returned anyway'));
        done();
    })

})

describe('Generic Authentication', function() {

    it('should not authenticate user without token', function(done) {
        var r = res();
        if(null !== auth(null, null, { cookie : false, header : false, query: false}, r)) return done(new Error('Was authenticated even if token was null'));
        if (r.statusCode !== 401) return done(new Error('statusCode should be 401'));
        done()
    })

    it('should authenticate all users with cookie', function(done) {
        for(var i=0; i<users.length; i++) {
            if(null === auth(users[i].token, null, { cookie : true, header : false, query: false})) return done(new Error('At least one user was not authenticated'));
        }
        done()
    })

    it('should authenticate all users with header', function(done) {
        for(var i=0; i<users.length; i++) {
            if(null === auth(users[i].token, null, { cookie : false, header : true, query: false})) return done(new Error('At least one user was not authenticated'));
        }
        done()
    })

    it('should authenticate all users with query param', function(done) {
        for(var i=0; i<users.length; i++) {
            if(null === auth(users[i].token, null, { cookie : false, header : false, query: true})) return done(new Error('At least one user was not authenticated'));
        }
        done()
    })

    it('should set req.user', function(done) {
        var request = req(users[0].token);
        worf.authenticate()(request, res(), ()=>{});
        if(!request.user || request.user.id != users[0].data.id) return done(new Error('Did not set req.user properly'))
        done()
    })

})


describe('Authentication options: general', function() {

    it('should authenticate user without token with { optional : true }', function(done) {
        var r = res();
        if(true !== auth(null, {optional : true}, null, r)) return done(new Error('Not passed'));
        if (r.statusCode !== 200) return done(new Error('statusCode should be 200'));
        done()
    })

    it('should not authenticate when changing secret', function(done) {
        worf.setSecret('tests');
        var user = auth(users[0].token);
        var error = null === user ? undefined : new Error('Was able to authenticate anyway');
        worf.setSecret(SECRET);
        done(error)
    })

})


describe('Authentication options: basic require', function() {

    it('should authenticate only user 1 with { id : 1 } ', function(done) {
        var r = res();
        if(null === auth(users[0].token, { require : {id:1}})) return done(new Error('User 1 was not autheticated'));
        if(null !== auth(users[1].token, { require : {id:1}}, null, r)) return done(new Error('User 2 was autheticated anyway'));
        if (r.statusCode != 403) return done(new Error('statusCode should be 403'));
        if(null !== auth(users[2].token, { require : {id:1}})) return done(new Error('User 3 was autheticated anyway'));
        done()
    })

    it('should authenticate only user 1 with { id : { eq : 1 } } ', function(done) {
        if(null === auth(users[0].token, { require : { id : { eq : 1 } } })) return done(new Error('User 1 was not autheticated'));
        if(null !== auth(users[1].token, { require : { id : { eq : 1 } } })) return done(new Error('User 2 was autheticated anyway'));
        if(null !== auth(users[2].token, { require : { id : { eq : 1 } } })) return done(new Error('User 3 was autheticated anyway'));
        done()
    })

    it('should authenticate users 1,3 with { id : { neq : 1 } } ', function(done) {
        if(null !== auth(users[0].token, { require : { id : { neq : 1 } } })) return done(new Error('User 1 was autheticated anyway'));
        if(null === auth(users[1].token, { require : { id : { neq : 1 } } })) return done(new Error('User 2 was not autheticated'));
        if(null === auth(users[2].token, { require : { id : { neq : 1 } } })) return done(new Error('User 3 was not autheticated'));
        done()
    })

    it('should authenticate users 1,2 with { id : { in : [1,2] } } ', function(done) {
        if(null === auth(users[0].token, { require : { id : { in : [1,2] } } })) return done(new Error('User 1 was not autheticated'));
        if(null === auth(users[1].token, { require : { id : { in : [1,2] } } })) return done(new Error('User 2 was not autheticated'));
        if(null !== auth(users[2].token, { require : { id : { in : [1,2] } } })) return done(new Error('User 3 was autheticated anyway'));
        done()
    })

    it('should authenticate users 2,3 with { id : { gt : 1 } } ', function(done) {
        if(null !== auth(users[0].token, { require : { id : { gt : 1 } } })) return done(new Error('User 1 was autheticated anyway'));
        if(null === auth(users[1].token, { require : { id : { gt : 1 } } })) return done(new Error('User 2 was not autheticated'));
        if(null === auth(users[2].token, { require : { id : { gt : 1 } } })) return done(new Error('User 3 was not autheticated'));
        done()
    })

    it('should authenticate users 2,3 with { id : { gte : 2 } } ', function(done) {
        if(null !== auth(users[0].token, { require : { id : { gte : 2 } } })) return done(new Error('User 1 was autheticated anyway'));
        if(null === auth(users[1].token, { require : { id : { gte : 2 } } })) return done(new Error('User 2 was not autheticated'));
        if(null === auth(users[2].token, { require : { id : { gte : 2 } } })) return done(new Error('User 3 was not autheticated'));
        done()
    })


    it('should authenticate users 1,2 with { id : { lt : 3 } } ', function(done) {
        if(null === auth(users[0].token, { require : { id : { lt : 3 } } })) return done(new Error('User 1 was not autheticated'));
        if(null === auth(users[1].token, { require : { id : { lt : 3 } } })) return done(new Error('User 2 was not autheticated'));
        if(null !== auth(users[2].token, { require : { id : { lt : 3 } } })) return done(new Error('User 3 was autheticated anyway'));
        done()
    })

    it('should authenticate users 1,2 with { id : { lte : 2 } } ', function(done) {
        if(null === auth(users[0].token, { require : { id : { lte : 2 } } })) return done(new Error('User 1 was not autheticated'));
        if(null === auth(users[1].token, { require : { id : { lte : 2 } } })) return done(new Error('User 2 was not autheticated'));
        if(null !== auth(users[2].token, { require : { id : { lte : 2 } } })) return done(new Error('User 3 was autheticated anyway'));
        done()
    })

})


describe('Authentication options: advanced require with lists', function() {

    it('should authenticate users 2,3 with { role : { gt : "standard" } } ', function(done) {
        if(null !== auth(users[0].token, { require : { role : { gt : "standard" } } })) return done(new Error('User 1 was autheticated anyway'));
        if(null === auth(users[1].token, { require : { role : { gt : "standard" } } })) return done(new Error('User 2 was not autheticated'));
        if(null === auth(users[2].token, { require : { role : { gt : "standard" } } })) return done(new Error('User 3 was not autheticated'));
        done()
    })

    it('should authenticate users 2,3 with { role : { gte : "moderator" } } ', function(done) {
        if(null !== auth(users[0].token, { require : { role : { gte : "moderator" } } })) return done(new Error('User 1 was autheticated anyway'));
        if(null === auth(users[1].token, { require : { role : { gte : "moderator" } } })) return done(new Error('User 2 was not autheticated'));
        if(null === auth(users[2].token, { require : { role : { gte : "moderator" } } })) return done(new Error('User 3 was not autheticated'));
        done()
    })

    it('should authenticate users 1,2 with { role : { lt : "admin" } } ', function(done) {
        if(null === auth(users[0].token, { require : { role : { lt : "admin"  } } })) return done(new Error('User 1 was not autheticated'));
        if(null === auth(users[1].token, { require : { role : { lt : "admin"  } } })) return done(new Error('User 2 was not autheticated'));
        if(null !== auth(users[2].token, { require : { role : { lt : "admin"  } } })) return done(new Error('User 3 was autheticated anyway'));
        done()
    })

    it('should authenticate users 1,2 with { role : { lte : "moderator" } } ', function(done) {
        if(null === auth(users[0].token, { require : { role : { lte : "moderator"  } } })) return done(new Error('User 1 was not autheticated'));
        if(null === auth(users[1].token, { require : { role : { lte : "moderator"  } } })) return done(new Error('User 2 was not autheticated'));
        if(null !== auth(users[2].token, { require : { role : { lte : "moderator"  } } })) return done(new Error('User 3 was autheticated anyway'));
        done()
    })

})

describe('User object', function() {

    it('should return true when calling req.user.field("id").gt(1) on users 2,3', function(done) {
        if(true  === auth(users[0].token).field('id').gt(1)) return done(new Error('Failed on user 1'));
        if(false === auth(users[1].token).field('id').gt(1)) return done(new Error('Failed on user 1'));
        if(false === auth(users[2].token).field('id').gt(1)) return done(new Error('Failed on user 1'));
        done()
    })

    it('should return true when calling req.user.field("id").gte(2) on users 2,3', function(done) {
        if(true  === auth(users[0].token).field('id').gte(2)) return done(new Error('Failed on user 1'));
        if(false === auth(users[1].token).field('id').gte(2)) return done(new Error('Failed on user 1'));
        if(false === auth(users[2].token).field('id').gte(2)) return done(new Error('Failed on user 1'));
        done()
    })


    it('should return true when calling req.user.field("id").lt(3) on users 1,2', function(done) {
        if(false === auth(users[0].token).field('id').lt(3)) return done(new Error('Failed on user 1'));
        if(false === auth(users[1].token).field('id').lt(3)) return done(new Error('Failed on user 1'));
        if(true  === auth(users[2].token).field('id').lt(3)) return done(new Error('Failed on user 1'));
        done()
    })

    it('should return true when calling req.user.field("id").lte(2) on users 1,2', function(done) {
        if(false === auth(users[0].token).field('id').lte(2)) return done(new Error('Failed on user 1'));
        if(false === auth(users[1].token).field('id').lte(2)) return done(new Error('Failed on user 1'));
        if(true  === auth(users[2].token).field('id').lte(2)) return done(new Error('Failed on user 1'));
        done()
    })


    it('should return true when calling req.user.field("role").gt("standard") on users 2,3', function(done) {
        if(true  === auth(users[0].token).field('role').gt("standard")) return done(new Error('Failed on user 1'));
        if(false === auth(users[1].token).field('role').gt("standard")) return done(new Error('Failed on user 1'));
        if(false === auth(users[2].token).field('role').gt("standard")) return done(new Error('Failed on user 1'));
        done()
    })

    it('should return true when calling req.user.field("role").gte("moderator") on users 2,3', function(done) {
        if(true  === auth(users[0].token).field("role").gte("moderator")) return done(new Error('Failed on user 1'));
        if(false === auth(users[1].token).field("role").gte("moderator")) return done(new Error('Failed on user 1'));
        if(false === auth(users[2].token).field("role").gte("moderator")) return done(new Error('Failed on user 1'));
        done()
    })


    it('should return true when calling req.user.field("role").lt("admin") on users 1,2', function(done) {
        if(false === auth(users[0].token).field("role").lt("admin")) return done(new Error('Failed on user 1'));
        if(false === auth(users[1].token).field("role").lt("admin")) return done(new Error('Failed on user 1'));
        if(true  === auth(users[2].token).field("role").lt("admin")) return done(new Error('Failed on user 1'));
        done()
    })

    it('should return true when calling req.user.field("role").lte("moderator") on users 1,2', function(done) {
        if(false === auth(users[0].token).field("role").lte("moderator")) return done(new Error('Failed on user 1'));
        if(false === auth(users[1].token).field("role").lte("moderator")) return done(new Error('Failed on user 1'));
        if(true  === auth(users[2].token).field("role").lte("moderator")) return done(new Error('Failed on user 1'));
        done()
    })

    it("should wrap a generic object in a Worf object", function(done) {
        var user = new worf.User(users[0].data);
        if(user.id != users[0].data.id) return done(new Error('Ids do not match'));
        if(user.role != users[0].data.role) return done(new Error('Roles do not match'));
        if(!user.field) return done(new Error('field() method not present'));
        if(true !== user.field('role').lt('admin')) return done(new Error('field() method not working'));
        if(false !== user.field('role').gt('admin')) return done(new Error('field() method not working'));
        if(true !== user.field('role').gte('standard')) return done(new Error('field() method not working'));
        done()
    })

})


describe('Secret Overwrite', function(done) {
    it('should use an alternative secret to encode a token with encode()', function(done) {
        var token = worf.encode({id:1}, undefined, SECRET_OW);
        var decoded = worf.decode(token);

        if(decoded !== null) return done(new Error('The token was decoded with old secret'));
        decoded = worf.decode(token, SECRET_OW);
        if(decoded === null) return done(new Error('Could not decode with same secret'));
        done();
    });

    it('should use an alternative secret to encode a token and should fail to authenticate', function(done) {
        var token = worf.encode({id:1}, undefined, SECRET_OW);
        if(null !== auth(token)) return done(new Error('User was autheticated'));
        done();
    });

    it('should use an alternative secret to encode a token and authenticate with options.secret.require', function(done) {
        var token = worf.encode({id:1}, undefined, SECRET_OW);
        if(null !== auth(token)) return done(new Error('User was authenticated with global secret'));
        if(null === auth(token, { secret : SECRET_OW })) return done(new Error('User was not authenticated'));
        done();
    });

    it('should use an alternative secret to encode a token and authenticate with options.secret.alternative, also with a token encode with global secret', function(done) {
        var token1 = worf.encode({id:1});
        var token2 = worf.encode({id:2}, undefined, SECRET_OW);
        if(null === auth(token1, { secret : { alternative:SECRET_OW } })) return done(new Error('User 1 was not authenticated'));
        if(null === auth(token2, { secret : { alternative:SECRET_OW } })) return done(new Error('User 2 was not authenticated'));
        done();
    });
})


describe('Expiration', function(done) {
    it('should encode a token that expires in 3 seconds with encode()', function(done) {
        this.timeout(5000);
        var token = worf.encode({id:1}, '2s');
        var decoded = worf.decode(token);
        if(!decoded) return done(new Error('Could not decode token'));

        setTimeout(function() {
            var decoded = worf.decode(token);
            if(decoded) return done(new Error('Token was decoded even if expired'));
            done();
        }, 3500);
    });

    it('should not authenticate with an expired token', function(done) {
        this.timeout(5000);
        var token = worf.encode({id:1}, '2s');
        if(null === auth(token)) return done(new Error('User was not authenticated'));

        setTimeout(function() {
            if(null !== auth(token)) return done(new Error('User was authenticated'));
            done();
        }, 3500);
    });
})
