const urlParser = require("url");

var compareToList = module.exports.compareToList = function(user, lists, fn, field, val) {
    var targetValue = lists[field] ? lists[field].indexOf(val) : val;
    var sourceValue = lists[field] ? lists[field].indexOf(user[field]) : user[field];

    switch (fn) {
        case 'gte': return sourceValue >= targetValue;
        case 'gt': return sourceValue > targetValue;
        case 'lte': return sourceValue <= targetValue;
        case 'lt': return sourceValue < targetValue;
    }
}


var checkRequire = module.exports.checkRequire = function(user, field, value) {
    if(typeof value != 'object') return user[field] == value;
    var fn = Object.keys(value)[0];
    if('in' == fn) return value[fn].includes(user[field]);
    if('eq' == fn) return value[fn] === user[field];
    if('neq' == fn) return value[fn] !== user[field];
    if(['gte', 'lte', 'gt', 'lt'].includes(fn)) return user.field(field)[fn](value[fn]);
    return false;
}

var getTokenFromRequest = module.exports.getTokenFromRequest = function(req, tokenKey) {
    var token;

    if(null == (token = getTokenFromCookies(req, tokenKey))) {
        //console.log('worf: Token not found in cookie (searching for "' + tokenKey + '" cookie)');
        if(null == (token = getTokenFromHeader(req))) {
            //console.log('worf: Token not found in Authorization header');
            if(null == (token = getTokenFromUrlQuery(req, tokenKey))) {
                //console.log('worf: Token not found. Request not authenticated.');
                return null;
            }
        }
    }

    return token;
}

var getTokenFromCookies = module.exports.getTokenFromCookies = function(req, tokenKey){
    return !req.cookies || !req.cookies[tokenKey] ? null : req.cookies[tokenKey];
}

var getTokenFromHeader = module.exports.getTokenFromHeader = function(req){
    if(!req.headers || !req.headers.authorization) return null;
    return req.headers.authorization;
}

var getTokenFromUrlQuery = module.exports.getTokenFromUrlQuery = function(req, tokenKey){
    if(!req.url) return null;
    var url = urlParser.parse(req.url, true);
    if(!url.query || !url.query[tokenKey]) return null;
    return url.query[tokenKey];
}
