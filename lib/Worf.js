const jwt = require('jsonwebtoken');
const internal = require('./internal');

/**
 * Worf
 */
var Worf = function() {
    this.secret = undefined;
    this.tokenKey = 'token';
    this.debug = false;
    this.lists = {};
    this.defaultError = 401;

    var parent = this;

    this.User = function(source) {
        for(var x in source) this[x] = source[x];
    }

    this.User.prototype.field = function(field) {
        var self = this;
        return {
            gte : (val) => internal.compareToList(self, parent.lists, 'gte', field, val),
            gt :  (val) => internal.compareToList(self, parent.lists, 'gt', field, val),
            lte : (val) => internal.compareToList(self, parent.lists, 'lte', field, val),
            lt :  (val) => internal.compareToList(self, parent.lists, 'lt', field, val),
        }
    }
};



/**
 * @abstract returns a middleware function to be used in express-based apps.
 * @param {Object} options  : configures the middleware. it is optional and can have the following fields:
 * @param {Object} [options.optional=false] : bool, whether the middleware should let the request pass even if no valid
 *                                   token is found (in this case req.user is null). Default: false
 * @param {Object} [options.require] : object, authenticates only if all fields in the decoded user object match
 *                                   the requested. e.g.:
 *                                      worf.authenticate({ require : { id: 1, level : 'admin' } })
 *                                   only authorizes user with id 1 AND level 'admin' to access the route.
 * @param {Object} [options.error=403] : the error code to return in case of user not authenticated. Default: 403
 * @param {Object|String} [options.secret] : overwrites token secret for this route. Object with ONE of the two possible keys:
 *          - {String} options.secret.require : tries to decode the token with this secret, disregarding global secret set via config.
 *          - {String} options.secret.alternative : tries to decode the token with the global secret, then tries this alternative in case of failure.
 * NOTE: if options.secret is a String, it is treated as 'require'
 */
Worf.prototype.authenticate = function(options){
    options = options || {};
    options.optional = options.optional !== undefined ? options.optional : false;
    options.error = options.error || this.defaultError;

    var self = this;

    options.secret = options.secret || {};

    if(typeof options.secret === 'string') options.secret = { require : options.secret };

    return function(req, res, next) {
        //console.log("worf.authenticate");
        var specificError = undefined;

        var token = internal.getTokenFromRequest(req, self.tokenKey);
        var user = self.decode(token, options.secret.require); // options.secret.required could be undefined, in which case the global is used

        if(!user && options.secret.alternative) user = self.decode(token, options.secret.alternative);

        if(user && (typeof options.require == 'object')) {
            for(var x in options.require) {
                if(!internal.checkRequire(user, x, options.require[x])) {
                    user = null;
                    specificError = 403;
                    break;
                }
            }
        }

        if(!user && options.optional) user = true;

        if(!user) {
            res.statusCode = specificError ? specificError : options.error;
            res.end();
            return null; // for tests
        }

        req.user = user === true ? null : user;
        next();
        return user; // for tests
    }
}




/**
 * Autenthication method. Returns the user if a valid
 * token was found in the request cookie or authorization header,
 * null otherwise.
 * @param {Request} req : the request to evaluate
 */
Worf.prototype.getUser = function(req) {
	var token = internal.getTokenFromRequest(req, this.tokenKey);
    return token ? this.decode(token) : null;
}



/**
* @abstract sets configuration for the instance
*
*/
Worf.prototype.config = function(options) {
    options = options || {};
    if(options.tokenKey) this.tokenKey = options.tokenKey;
    if(options.errorCode) this.defaultError = options.errorCode;
    if(options.secret) this.secret = options.secret;
    if(options.lists) this.lists = options.lists;
}

Worf.prototype.setSecret = function(jwtsecret) { this.secret = jwtsecret; }
Worf.prototype.setErrorCode = function(code) { this.defaultError = code; }
Worf.prototype.enableDebug = function() { this.debug = true; }
Worf.prototype.setTokenKey = function(key){ this.tokenKey = key; }
Worf.prototype.addList = function(fieldName, values){ this.lists[fieldName] = value; }





Worf.prototype.encode = function(user, expiresIn, secret) {
    if(secret === undefined) secret = this.secret;
    expiresIn = expiresIn || '30d';
    return jwt.sign({obj: user}, secret, {expiresIn: expiresIn});
}

Worf.prototype.decode = function(token, secret) {
    if(secret === undefined) secret = this.secret;

	try {
		var decoded = jwt.verify(token, secret);
	    //console.log("decoded", decoded);
        if(!decoded.obj) return null;
        decoded.obj.token = token;
        return new this.User(decoded.obj, this.lists);
	} catch (err) {
		//console.error(err);
		return null;
	}
}


module.exports = Worf;
